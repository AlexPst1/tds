// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "../FuncLibrary/TDSTypes.h"
#include "ProjectileDefault_Granade.generated.h"


UCLASS()
class TDS_API AProjectileDefault_Granade : public AProjectileDefault {

	GENERATED_BODY()


	

	

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);
	//FProjectileInfo ProjectileSetting;
		
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void ImpactProjectile() override;
	void Explose();

	bool TimerEnable = false;
	float TimerToExplose = 0.0f;
	float TimeToExplose = 5.0f;
};