// Fill out your copyright notice in the Description page of Project Settings.
#include "ProjectileDefault_Granade.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"



int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplode"),
	DebugExplodeShow, 
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);


// Called when the game starts or when spawned
void AProjectileDefault_Granade::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileDefault_Granade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);

}

void AProjectileDefault_Granade::TimerExplose(float DeltaTime) 
{
	if (TimerEnable) 
	{
		if (TimerToExplose > TimeToExplose) 
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}



void AProjectileDefault_Granade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpuls, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpuls, Hit);
	
}



void AProjectileDefault_Granade::ImpactProjectile()
{
	TimerEnable = true;
}

void AProjectileDefault_Granade::Explose() 
{
	TimerEnable = false;
	
	if (DebugExplodeShow) 
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), 100.0f, 30, FColor::Red, false, 5.0f, 2, 2);
		DrawDebugSphere(GetWorld(), GetActorLocation(), 200.0f, 30, FColor::Blue, false, 5.0f, 2, 2);
	}
	if (ProjectileSetting.ExploseFX) 
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage * 0.2f, 
		GetActorLocation(), 100.0f, 200.0f, 5, NULL, IgnoredActor, nullptr, nullptr);

	

	
	this->Destroy();
}