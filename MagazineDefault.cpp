// Fill out your copyright notice in the Description page of Project Settings.


#include "MagazineDefault.h"

// Sets default values
AMagazineDefault::AMagazineDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshMagazine = CreateDefaultSubobject <USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMeshMagazine->SetGenerateOverlapEvents(false);
	SkeletalMeshMagazine->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshMagazine->SetupAttachment(RootComponent);

	StaticMeshMagazine = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMagazineMesh"));
	StaticMeshMagazine->SetGenerateOverlapEvents(false);
	StaticMeshMagazine->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshMagazine->SetupAttachment(RootComponent);
	StaticMeshMagazine->SetStaticMesh(myMagazineMesh.MagazineDrop);
	//StaticMeshMagazine = myMagazineMesh.MagazineDrop;
}

// Called when the game starts or when spawned
void AMagazineDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMagazineDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

void AMagazineDefault::InitMagazine(UStaticMesh* Dropmesh, FTransform Offset, FVector DropImpulseDirection, float LifeTime, float ImpulsRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (SkeletalMeshMagazine && !SkeletalMeshMagazine->SkeletalMesh)
	{
		SkeletalMeshMagazine->DestroyComponent();
	}
	if (StaticMeshMagazine && !StaticMeshMagazine->GetStaticMesh())
	{
		StaticMeshMagazine->DestroyComponent();
	}
	
}

